const discord_utils = require('./utils/discordUtils');
const strings = require("./resources/strings-it");

function getChannel(guild, mention) {
  if (mention.startsWith('<#') && mention.endsWith('>')) {
    mention = mention.slice(2, -1);
    return guild.channels.get(mention);
  }
}

function onMessage(command, args, message) {
  // Check for user permissions
  if (!message.member.roles.has('549611536935813125')) {
    return;
  }
  switch (message.channel.name) {
    case "administration":
      switch (command) {
        case "test":
          switch (args[0]) {
            case "mail":
              discord_utils.test_mail(args[1]);
              message.reply(strings.message_mailSent.replace("%TO%", args[1]));
              break;
          }
          break;
        case "reply":
          message.reply(args[0]);
          break;
        case "say":
          message.channel.send(args.join(' '));
          break;
        case "sayin":
          // TODO message.guild.channels
          const channel = getChannel(message.guild, args[0])
          channel.send(args.slice(1).join(' '));
          break;
      }
      break;
  }
}

exports.init = function init(client) {
  return onMessage;
}