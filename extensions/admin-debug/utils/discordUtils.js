const properties = require("../resources/strings-it");
const gapi = require('../../../utils/gmailUtils');

function log(guild, message) {
  console.log(message);
  // Send the message to a designated channel on a server:
  const logChannel = guild.channels.find(ch => ch.name === 'log');
  // Do nothing if the channel wasn't found on this server
  if (!logChannel) return;
  // Send the message, mentioning the member
  logChannel.send(message);
}

function test_mail(to) {
  gapi.sendMail(to, "Mail test - server Discord di UNIBO", "Se leggi il messaggio, il test ha avuto successo.");
}

exports.test_mail = test_mail;