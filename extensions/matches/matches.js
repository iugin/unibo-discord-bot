const strings = require("./resources/strings-it");
const storage = require("./utils/storageUtils");

function getMessage(member, sport, date, matchId) {
  return storage.getParticipants(matchId)
    .then(participants => {
      var message = strings.message_partita
        .replace('%DATE%', date)
        .replace('%SPORT%', sport)
        .replace('%MEMBER%', member);
      participants.map(p => p.user).forEach(p => message = message.concat(`\n${p}`))
      return message;
    });
}

function messageMatch(channel, member, sport, date, matchId) {
  console.log(`Pubblicazione partita [${matchId}] di ${sport} il ${date} organizzata da ${member}`);
  getMessage(member, sport, date, matchId)
    .then(text => {
      channel.send(text)
        .then((message) => message.react('✅').then(() => message))
        .then((message) => message.react('❌').then(() => message))
        .then((message) => message.react('🔕').then(() => message))
        .then((message) => {
          const filter = (reaction, user) => {
            return ['✅', '❌', '🔕'].includes(reaction.emoji.name) && user.id != message.author.id;
          };
          message.awaitReactions(filter, { max: 1, time: 60000 * 60 * 24, errors: ['time'] })
            .then(collected => {
              const reaction = collected.first();
              const reactionLastAuthor = reaction.message.guild.member(reaction.users.last()).nickname;
              switch (reaction.emoji.name) {
                case '✅':
                  storage.addParticipation(matchId, reactionLastAuthor);
                  getMessage(member, sport, date, matchId).then(text => message.edit(text));
                  break;
                case '❌':
                  storage.removeParticipation(matchId, reactionLastAuthor);
                  getMessage(member, sport, date, matchId).then(text => message.edit(text));
                  break;
                case '🔕':
                  if (reactionLastAuthor == member) {
                    storage.deleteMatch(matchId);
                    channel.send(strings.message_match_deleted
                      .replace('%DATE%', date)
                      .replace('%SPORT%', sport));
                  } else {
                    channel.send(strings.message_match_cannot_delete
                      .replace('%NICKNAME%', reactionLastAuthor));
                  }
                  break;
              }
            })
        })
        
    });
}

function createMatch(member, sport, date) {
  console.log(`Creazione partita di ${sport} il ${date} organizzata da ${member}`);
  if (new Date(date) >= new Date()) {
    return storage.newMatch(member, sport, date);
  } else {
    return Promise.reject(strings.message_match_error_invalid_date.replace('%NICKNAME%', member));
  }
}

function listMatches(channel) {
  storage.getMatches().then(rows => {
    rows.forEach(row => {
      messageMatch(channel, row.user, row.sport, row.date, row.id);
    });
  });
}

function onMessage(command, args, message) {
  const member = message.member.nickname;
  switch (message.channel.name) {
    case "sport":
    case "attività":
      switch (command) {
        case "help":
          message.reply(strings.message_help);
          break;
        case "crea":
          createMatch(member, args[0], `${args[1]} ${args[2]}`)
            .then(id => messageMatch(message.channel, member, args[0], `${args[1]} ${args[2]}`, id))
            .catch(err => message.reply(err));
          break;
        case "partite":
          listMatches(message.channel);
          break;
      }
      break;
  }
}

exports.init = function init(client) {
  return onMessage;
}