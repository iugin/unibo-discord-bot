exports.message_help = `
Ehi, sono il bot! Se vuoi ti posso aiutare ad organizzare una partita.
I comandi sono:
* \`crea [SPORT] [DATA] [ORA]\` per creare una nuova partita; la data va inserita nel formato AAAA-MM-DD; l'ora nel formato HH:MM
* \`partite\` per ottenere la lista delle partite.

Inoltre si possono usare le reazioni:
:white_check_mark: per partecipare alla partita;
:x: per rimuovere la propria partecipazione dalla partita;
:no_bell: per eliminare l'evento (solo colui che lo ha creato lo può eliminare).

PS: Non scordarti di usare il prefisso davanti ai comandi :wink:
`;
exports.message_partita = `
Il %DATE% c'è la partita di %SPORT% organizzata da %MEMBER%.

Usa le reazioni!
:white_check_mark: per partecipare alla partita;
:x: per rimuovere la propria partecipazione dalla partita;
:no_bell: per eliminare l'evento (solo colui che lo ha creato lo può eliminare).

I giocatori attuali sono:
`;
exports.message_match_deleted = `
La partita di %SPORT% del %DATE% è stata annullata con successo. Peccato, sarebbe stata divertente :frowning:
`
exports.message_match_cannot_delete = `
Ehi %NICKNAME%, non hai mica organizzato tu la partita! Non puoi annullarla :angry:
`
exports.message_match_error_invalid_date = `
Occhio! La data che hai inserito non è valida.
`