var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('database/matches.db');

db.serialize(function () {
  db.run("CREATE TABLE IF NOT EXISTS matches (id INTEGER PRIMARY KEY AUTOINCREMENT, user TEXT, sport TEXT, date TEXT)", (res, err) => { });
  db.run("CREATE TABLE IF NOT EXISTS participations (match INTEGER NOT NULL, user TEXT NOT NULL, PRIMARY KEY (match, user))", (res, err) => { });
});

function getMatches() {
  // let now = new Date(); TODO
  // WHERE date >= '" + `${now.getDate()}-${now.getMonth()}-${now.getFullYear()}` + "'
  return new Promise((resolve, reject) => {
    db.all(`SELECT * FROM matches`, function (err, rows) {
      if (err) {
        reject();
      } else {
        rows
          // Elimino le partite passate
          .filter(row => new Date(row.date) < new Date())
          .forEach(row => deleteMatch(row.id));
        // Restituisco le prossime partite
        resolve(rows.filter(row => new Date(row.date) >= new Date()));
      }
    });
  })
}

function getParticipants(matchId) {
  return new Promise((resolve, reject) => {
    db.all("SELECT * FROM participations WHERE match==" + matchId, function (err, rows) {
      if (err) {
        reject();
      } else {
        resolve(rows);
      }
    });
  })
}

function newMatch(user, sport, date) {
  return new Promise((resolve, reject) => {
    db.run('INSERT INTO matches(user, sport, date) VALUES(?, ?, ?)',
      [user, sport, date], function (err) {
        if (err) {
          return reject();
        } else {
          return resolve(this.lastID);
        }
      });
  })
}

function deleteMatch(matchId) {
  db.run(`DELETE FROM matches WHERE id=='${matchId}'`, O_o => { });
  db.run(`DELETE FROM participations WHERE match=='${matchId}'`, O_o => { });
}

function addParticipation(matchId, user) {
  db.run('INSERT INTO participations(match, user) VALUES(?, ?)',
    [matchId, user], O_o => { });
}

function removeParticipation(matchId, user) {
  db.run(`DELETE FROM participations WHERE match==${matchId} AND user=='${user}'`, O_o => { });
}

exports.getMatches = getMatches;
exports.newMatch = newMatch;
exports.deleteMatch = deleteMatch;
exports.addParticipation = addParticipation;
exports.removeParticipation = removeParticipation;
exports.getParticipants = getParticipants;