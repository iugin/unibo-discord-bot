exports.init = function init(client) {
  client.on('guildMemberAdd', member => {
    console.log(`New member entered: ${member.displayName}`);
    require('./utils/discordUtils').registerUser(member.guild, member)
  });
  
  client.on('guildMemberRemove', member => {
    console.log(`Member left: ${member.displayName}`);
    require('./utils/discordUtils').removeUser(member.guild, member);
  });
}