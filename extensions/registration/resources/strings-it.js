const config = require("../../../config/config.json");
message_support = 'Per qualunque problema potete ricevere supporto contattando: ' + config.support.join(', ');
discord_link = 'https://discord.gg/Apurtbk';
exports.message_rules = `
Benvenuto al server discord NON ufficiale dell' **Università degli Studi di Bologna**!
Stiamo ancora definendo le regole ufficiali, per ora:
[] rispetta gli altri
[] non spammare se non nei canali appositi
[] usa i canali esclusivamente per quello per cui sono stati progettati

Vi è una sezione per quanto riguarda i **corsi** ed una per i **giochi**. Se vuoi un nuovo canale, richiedilo agli amministratori ( @admin ).

Per tutto il resto, ti chiediamo di rispettare quanto dettato dal tuo buonsenso.

Per poter entrare devi accettare, accetti?
`;
exports.message_mailRequest = 'Digita il tuo indirizzo mail universitario (non viene in alcun modo salvato sui server, viene utilizzato esclusivamente per inviare il codice di conferma)';
exports.message_verificationCodeRequest = 'Ti è stato inviato un codice di conferma via mail, hai 5 minuti per inserirlo';
exports.message_registrationCompleted = 'Verifica completata, ti è stato assegnato il ruolo di: %ROLE%. Bevenuto/a!';
exports.message_error_timedOut = 'Hai impiegato troppo a rispondere, termino la configurazione. Riprova ad accedere: ' + discord_link + '. ' + message_support;
exports.message_error_timedOut_awaitrules = '**Attento!** Devi accettare le regole prima di poter accedere. ' + exports.message_error_timedOut;
exports.message_error_timedOut_awaitmail = '**Attento!** Quando richiesto devi fornire la tua mail istituzionale per verificare l\'iscrizione. ' + exports.message_error_timedOut;
exports.message_error_timedOut_awaittoken = '**Attento!** Devi fornire il token di conferma ricevuto via mail. ' + exports.message_error_timedOut;
exports.message_error_refusedRules = 'Non puoi accedere al server in quanto hai rifiutato le regole, ci dispiace.';
exports.message_error_mailInvalid = 'La mail inserita non è valida, termino la configurazione. Riprova ad accedere: ' + discord_link + '.';
exports.message_error_mailRegistered = 'Utente già registrato, termino la registrazione. ' + message_support;
exports.message_error_invalidCode = 'Il codice inseito è errato, termino la registrazione. Per riprovare: ' + discord_link + '.' + message_support;
exports.message_error_problemOnRole = 'Errore durante l\'assegnazione del ruolo nel server, riprova più tardi (' + discord_link + ') o contatta un amministratore. ' + message_support;
exports.message_error_generic = 'Errore generico, termino la configurazione. Riprova più tardi (' + discord_link + ') o contatta un amministratore. ' + message_support;
