const properties = require("../resources/strings-it");
const registrationUtils = require("./registrationUtils");

function log(guild, message) {
  console.log(message);
  // Send the message to a designated channel on a server:
  const logChannel = guild.channels.find(ch => ch.name === 'log');
  // Do nothing if the channel wasn't found on this server
  if (!logChannel) return;
  // Send the message, mentioning the member
  logChannel.send(message);
}

function kick(member, cause) {
  return member.kick(cause)
    .catch(error => log(guild, `Sorry, I couldn't kick ${member} because of : ${error}`));
}

function registerUser(guild, member) {
  member.createDM()
    .then((channel) =>
      // ------> Richiesta accettazione regole <------
      channel.send(properties.message_rules)
        .then((message) => message.react('✅').then(() => message))
        .then((message) => message.react('❌').then(() => message))
        .then((message) => {
          // Attendo che accetti le regole
          const filter = (reaction, user) => {
            return ['✅', '❌'].includes(reaction.emoji.name) && member.id === user.id;
          };
          return message.awaitReactions(filter, { max: 1, time: 300000, errors: ['time-awaitrules'] })
        })
        .then(collected => {
          const reaction = collected.first();
          switch (reaction.emoji.name) {
            case '❌':
              return Promise.reject('refused-rules');
              break;
            case '✅':
              return Promise.resolve();
              break;
          }
        })
        // ------> Richiesta inserimento mail <------
        .then(() => channel.send(properties.message_mailRequest))
        .then(() =>
          // Attendo che l'utente risponda
          channel.awaitMessages(response => response.author.id == member.id, {
            max: 1,
            time: 300000, // 5 minuti
            errors: ['time-awaitmail'],
          }))
        .then((collected) => {
          // Ricevuta la risposta che teoricamente contiene l'indirizzo
          // Valido la mail
          const mail = registrationUtils.validateUniboEmail(collected.first().content);
          return mail == undefined ? Promise.reject("mail-validation") : Promise.resolve(mail);
        })
        .then((mail) =>
          // Invio il codice di conferma
          new Promise((resolve, reject) => {
            registrationUtils.verifyUniboEmailOwnership(mail, member.id, verificationCode => {
              if (verificationCode == undefined) {
                reject("mail-registered");
              } else {
                resolve(verificationCode);
              }
            })
          })
            // ------> Codice di verifica <------
            .then((verificationCode) => channel.send(properties.message_verificationCodeRequest)
              .then(() =>
                // Attendo un messaggio dall'utente
                channel.awaitMessages(response => response.author.id == member.id, {
                  max: 1,
                  time: 300000,
                  errors: ['time-awaittoken'],
                }))
              .then((collected) => {
                // Messaggio ricevuto, verifico che il codice sia valido
                return collected.first().content == verificationCode ? Promise.resolve() : Promise.reject("invalid-code");
              }))
            .then(() =>
              // Assegno i permessi
              new Promise((resolve, reject) => {
                registrationUtils.updatePermissions(mail, member.id, guild, (role) => {
                  if (role == undefined) {
                    reject('role-assignation')
                  } else {
                    resolve(role);
                  }
                })
              })))
        .then((role) => channel.send(properties.message_registrationCompleted.replace('%ROLE%', role)))
        .then(() => {
          // Send the message to a designated channel on a server:
          const logChannel = guild.channels.find(ch => ch.name === 'member-log');
          // Do nothing if the channel wasn't found on this server
          if (!logChannel) return;
          // Send the message, mentioning the member
          logChannel.send(`New member joined: ${member}`);
        })
        .catch((error) => {
          cause = undefined;
          switch (error) {
            case 'time':
              cause = properties.message_error_timedOut
              break;
            case 'time-awaitrules':
              cause = properties.message_error_timedOut_awaitrules
              break;
            case 'time-awaitmail':
              cause = properties.message_error_timedOut_awaitmail
              break;
            case 'time-awaittoken':
              cause = properties.message_error_timedOut_awaittoken
              break;
            case 'refused-rules':
              cause = properties.message_error_refusedRules;
              break;
            case 'mail-validation':
              cause = properties.message_error_mailInvalid;
              break;
            case 'mail-registered':
              cause = properties.message_error_mailRegistered;
              break;
            case 'role-assignation':
              cause = properties.message_error_problemOnRole;
              break;
            case 'invalid-code':
              cause = properties.message_error_invalidCode;
              break;
            default:
              cause = properties.message_error_timedOut;
          }
          log(guild, `
          Error durign validation of ${member}: ${cause}
          Error message: ${error}
          `);
          channel.send(cause)
            .then(message => {
              setTimeout(function () {
                kick(member, cause);
              }, 1000)
            });
        }))
    .catch((error) => {
      log(guild, `Error durign validation of ${member}: ${JSON.stringify(error)}`);
      channel.send(properties.message_error_generic)
        .then(message => {
          setTimeout(function () {
            kick(member, cause);
          }, 1000)
        });
    });
}

function removeUser(guild, member) {
  registrationUtils.removeUserRegistration(member.id);
}

exports.registerUser = registerUser;
exports.removeUser = removeUser;