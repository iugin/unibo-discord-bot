const gapi = require('../../../utils/gmailUtils');
const random = require("../../../utils/randomUtils");
const storage = require("./storageUtils");

function validateUniboEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((studio.unibo.it)|(unibo.it))$/;
  return re.test(email) ? email : undefined;
}

function verifyUniboEmailOwnership(mail, userId, callback) {
  storage.alreadyRegistered(userId, mail, registered => {
    if (registered) {
      // Se è già registrato
      callback(undefined);
    } else {
      // Se non è già registrato
      // Genero un codice di verifica randomico
      const verificationCode = random.next();
      // Invio la mail
      gapi.sendMail(mail, "Registrazione al server Discord di UNIBO", "Il tuo codice di verifica è: " + verificationCode);
      callback(verificationCode);
    }
  });
}

function updatePermissions(mail, userId, guild, callback) {
  var roleName = undefined;
  if (mail.endsWith("@studio.unibo.it")) {
    // Se studente
    roleName = "studente";
  } else if (mail.endsWith("@unibo.it")) {
    // Se non studente
    roleName = "docente";
  } else {
    callback(undefined);
    return;
  }

  // Get the role
  let role = guild.roles.find(x => x.name === roleName);
  let user = guild.members.find(x => x.id === userId);
  if (!role || role === null || !user || user === null) {
    console.log("User or role not found");
    callback(undefined);
    return;
  }
  user.addRole(role)
    .then(() => {
      // Aggiorno il nickname
      let tmp = mail.split(".", 2);
      let nickname = capitalizeFirstLetter(tmp[0]) + " " + capitalizeFirstLetter(tmp[1].substring(0,3));
      return user.setNickname(nickname, 'e-mail verification')
    })
    .then(() => {
      // Salvo i dati in maniera sicura (onde evitare riuso della mail)
      return storage.registerUser(userId, mail);
    })
    .then(() => {
      // Informo del completamento
      callback(roleName);
    })
    .catch(err => {
      callback(undefined);
      console.log(err);
    });
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function removeUserRegistration(userId) {
  return storage.removeUser(userId);
}

module.exports.validateUniboEmail = validateUniboEmail;
module.exports.verifyUniboEmailOwnership = verifyUniboEmailOwnership;
module.exports.updatePermissions = updatePermissions;
module.exports.removeUserRegistration = removeUserRegistration;