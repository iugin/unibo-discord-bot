var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('database.sql');
var sjcl = require('sjcl');
var Base64 = require('js-base64').Base64;

db.serialize(function () {
  db.run("CREATE TABLE IF NOT EXISTS users (user TEXT, mail TEXT)", (res,err) => {});
});

function unidirectionalHash(input) {
  return Base64.encodeURI(sjcl.hash.sha256.hash(input));
}

function alreadyRegistered(user, mail, callback) {
  db.serialize(function () {
    db.get("SELECT count(*) as c FROM users WHERE mail=='" + unidirectionalHash(mail) + "' or user=='" + unidirectionalHash(user) + "'", function (err, row) {
      if (row.c == 0) callback(false)
      else callback(true);
    });
  });
}

function registerUser(user, mail) {
  db.run('INSERT INTO users(user, mail) VALUES(?, ?)',
    [unidirectionalHash(user), unidirectionalHash(mail)]);
}

function removeUser(userId) {
  db.run(`DELETE FROM users WHERE user=='${unidirectionalHash(userId)}'`, O_o => {});
}

exports.registerUser = registerUser;
exports.alreadyRegistered = alreadyRegistered;
exports.removeUser = removeUser;