exports.message_help = `
Ehi, sono il bot! Se vuoi ti posso aiutare a creare un canale privato per il tuo team.
I comandi sono:
* \`crea [NOME]\` per creare un nuovo team (canale testuale e vocale); questo scadrà tra un mese se non rinnovato (tranquillo, tornerò io a contattarti :blink: );
* \`elimina\` per rimuovere il team nel quale lo scrivi (può solo il proprietario);
* \`aggiungi [TAG/ID]\` per aggiungere al tuo team altri utento (può solo il proprietario);
* \`rimuovi [TAG/ID]\` per rimuovere utenti dal team (può solo il proprietario).

PS: Non scordarti di usare il prefisso davanti ai comandi :wink:
`;
exports.message_teamCreated = `
Ben fatto! Il team è stato creato.
Ora puoi aggiungere altri utenti con il comando: \`aggiungi [TAG]\`; il *TAG* è l'identificatore dell'utente, lo vedi cliccando sul suo nome (es. UTENTE#0000).
`;
exports.message_memberAdded = `
Sim sala bim! E %USER% è nel team!
`;
exports.message_memberRemoved = `
%USER% è stato rimosso :police_car:
`;
exports.message_error_invalidValue = `
Il valore inserito non è valido, controlla di averlo scritto correttamente!
`;
exports.message_error_insufficentPermission = `
Non hai permessi sufficienti, solo il creatore del team può eseguire questo comando.
`;
exports.message_error_invalidUser = `
L'utente non è presente sul server, controlla di aver inserito il tag giusto!
`;
exports.message_error_duplicateChannelName = `
Il nome scelto è già usato, prova a cambiare nome.
`;