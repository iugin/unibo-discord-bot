const configuration = require("./resources/configuration");
const strings = require("./resources/strings-it");
const storage = require("./utils/storageUtils");
const discordUtils = require("./utils/discordUtils");

function createTeam(guild, member, name) {
  var memberName = member.username;
  console.log(`Creazione team '${name}' per ${memberName}.`);
  const channelName = `team-${name}-${memberName}`;
  const categoryId = configuration.categoryId;
  console.log(member.id)
  
  return new Promise((resolve, reject) => {
    // Check that the name is not duplicate
    storage.getTeams().then(rows => {
      if (rows.filter(row => row.name == channelName).length == 0) {
        resolve();
      } else {
        reject(strings.message_error_duplicateChannelName);
      }
    });
  })
    // Create the team channels on the server
    .then(() => discordUtils.createTeam(guild, channelName, categoryId, member.id))
    // Register the team on the storage
    .then((channelIds) => {
      console.log(`Created channels: ${channelIds}`);
      return storage.createTeam(member.id, channelName, JSON.stringify(channelIds))
        .then(() => channelIds[0]);
    })
}

function removeTeam(guild, channel) {
  console.log(`Eliminazione team ${channel.name}`);
  var promises = [];
  getChannelIds(channel.name).then(channelIds =>
    channelIds.forEach(channelId => {
      console.log(`Removing channel: ${channelId}`);
      promises.push(discordUtils.removechannel(guild, channelId));
    }))
  Promise.all(promises)
    .then(() => storage.removeTeam(channel.name))
    .catch(console.log)
}

function addMember(channel, member) {
  return getChannelIds(channel.name).then(channelIds =>
    channelIds.forEach(channelId => {
      console.log(`Adding member ${member} to ${channelId}`);
      discordUtils.addMember(channel.guild.channels.get(channelId), discordUtils.getUserId(channel.guild, member));
    }))
    .then(() => Promise.resolve(member))
}

function removeMember(channel, member) {
  return getChannelIds(channel.name).then(channelIds =>
    channelIds.forEach(channelId => {
      console.log(`Removing member ${member} from ${channelId}`);
      discordUtils.removeMember(channel.guild.channels.get(channelId), discordUtils.getUserId(channel.guild, member));
    }))
    .then(() => Promise.resolve(member))
}

function getChannelIds(channelName) {
  // Returns a promise of array of channelIds: fetch from databse the channels assigned to that name.
  return new Promise((resolve, reject) => {
    storage.getTeams().then(rows => {
      var channelIds = rows.filter(row => row.name == channelName).map(row => row.channelIds)[0];
      if (channelIds != undefined) {
        resolve(JSON.parse(channelIds));
      } else {
        reject("Channel not found");
      }
    });
  });
}

function validateTeamName(str) {
  var re = /^[A-z0-9]+$/;
  return re.test(str) ? Promise.resolve(str) : Promise.reject(strings.message_error_invalidValue);
}

function validateMemberName(str) {
  var re = /(^\@?[A-z0-9]+\#[0-9]+$)|(^\<\@\![0-9]+\>$)/; // tagName | @tagName | <@!id>
  return re.test(str) ? Promise.resolve(str) : Promise.reject(strings.message_error_invalidValue);
}

function checkPermissionToEdit(channel, member) {
  return new Promise((resolve, reject) => {
    storage.getTeams().then(rows => {
      if (rows.filter(row => row.name == channel.name && row.owner == member.id).length > 0) {
        resolve();
      } else {
        reject(strings.message_error_insufficentPermission);
      }
    });
  });
}

function logError(member, error, message) {
  console.log(`ERROR: ${member} caused ${error}`);
  console.log(error);
  message.reply(error);
}

function onMessage(command, args, message) {
  const member = message.member.user;
  const guild = message.guild;
  if (message.channel.parent != null && message.channel.parent.name == "teams") {
    // Comandi generici per i teams
    switch (command) {
      case "help":
        message.reply(strings.message_help);
        break;
    }
    if (message.channel.name == "teams-lobby") {
      // Comandi per la gestione dei team nella lobby
      switch (command) {
        case "crea":
          validateTeamName(args[0])
            .then(team => createTeam(guild, member, team))
            .then((channelId) => guild.channels.get(channelId).send(strings.message_teamCreated))
            .catch((err) => logError(member, err, message))
          break;
      }
    } else {
      // Comandi per la gestione del singolo team
      switch (command) {
        case "elimina":
          checkPermissionToEdit(message.channel, member)
            .then(() => removeTeam(guild, message.channel))
            .catch((err) => logError(member, err, message))
          break;
        case "aggiungi":
          checkPermissionToEdit(message.channel, member)
            .then(() => validateMemberName(args[0]))
            .then((m) => addMember(message.channel, m))
            .then((m) => message.channel.send(strings.message_memberAdded.replace("%USER%", m)))
            .catch((err) => logError(member, err, message))
          break;
        case "rimuovi":
          checkPermissionToEdit(message.channel, member)
            .then(() => validateMemberName(args[0]))
            .then((m) => removeMember(message.channel, m))
            .then((m) => message.channel.send(strings.message_memberRemoved.replace("%USER%", m)))
            .catch((err) => logError(member, err, message))
          break;
      }
    }
  }
}

exports.init = function init(client) {
  return onMessage;
}