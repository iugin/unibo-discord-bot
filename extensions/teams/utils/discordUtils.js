const configuration = require("../resources/configuration");
const strings = require("../resources/strings-it");

function createTeam(guild, name, categoryId, ownerId) {
  var channelIds = new Array();
  // Creo il canale testuale
  return guild.createChannel(name, 'text', [{
    id: configuration.botId,
    allow: [
      'VIEW_CHANNEL',
      'SEND_MESSAGES',
      'READ_MESSAGE_HISTORY'
    ]
  }])
    .then((channel) => {
      channelIds.push(channel.id);
      return channel.setParent(categoryId).then(() => addMember(channel, ownerId));
    })
    // Creo il canale vocale
    .then(() => {
      return guild.createChannel(name, 'voice', [{
        id: configuration.botId,
        allow: [
          'VIEW_CHANNEL',
          'CONNECT'
        ]
      }]);
    })
    .then((channel) => {
      channelIds.push(channel.id);
      return channel.setParent(categoryId).then(() => addMember(channel, ownerId));
    })
    .then(() => Promise.resolve(channelIds))
}

function removechannel(guild, channelId) {
  // Elimino il canale
  return guild.channels.get(channelId).delete();
}

function addMember(channel, member) {
  return channel.overwritePermissions(getUserId(channel.guild, member), {
    VIEW_CHANNEL: true,
    SEND_MESSAGES: true,
    READ_MESSAGE_HISTORY: true,
    CONNECT: true,
    SPEAK: true
  })
}

function removeMember(channel, memberId) {
  return channel.permissionOverwrites.get(memberId).delete();
}

function getUserId(guild, userNameIdTag) {
  var userId = userNameIdTag;
  if (userNameIdTag.includes("#")) {
    // User specified the tag name
    var user = guild.members.find(m => m.user.tag == userNameIdTag)
    if (user == null) return Promise.reject(strings.message_error_invalidUser);
    userId = user.id;
  } else if (userNameIdTag.includes("@")) {
    // User specified the id
    userId = userNameIdTag.substring(3, 21);
  }
  return userId;
}

exports.createTeam = createTeam;
exports.removechannel = removechannel;
exports.addMember = addMember;
exports.removeMember = removeMember;
exports.getUserId = getUserId;