var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('database/teams.db');

db.serialize(function () {
  db.run("CREATE TABLE IF NOT EXISTS teams (name TEXT PRIMARY KEY, owner TEXT, creationDate TEXT, channelIds TEXT)", (res, err) => { });
});

function getTeams() {
  return new Promise((resolve, reject) => {
    db.all(`SELECT * FROM teams`, function (err, rows) {
      if (err) {
        reject();
      } else {
        resolve(rows);
      }
    });
  })
}

function createTeam(teamOwner, teamName, channelIds) {
  return new Promise((resolve, reject) => {
    db.run('INSERT INTO teams(name, owner, creationDate, channelIds) VALUES(?, ?, ?, ?)',
      [teamName, teamOwner, new Date(), channelIds], function (err, rows) {
        if (err) {
          reject();
        } else {
          resolve(rows);
        }
      });
    });
}

function removeTeam(name) {
  return new Promise((resolve, reject) => {
    db.run(`DELETE FROM teams WHERE name=='${name}'`, function (err, rows) {
      if (err) {
        reject();
      } else {
        resolve(rows);
      }
    });
  });
}

exports.getTeams = getTeams;
exports.createTeam = createTeam;
exports.removeTeam = removeTeam;