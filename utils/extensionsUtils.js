
const extensionActionsOnMessage = [];

var client;

exports.setClient = function setClient(c) {
  client = c;
}

exports.add = function add(name) {
  const actionOnMessage = require(`../extensions/${name}/${name}`).init(client);
  if (actionOnMessage != undefined) {
    extensionActionsOnMessage.push(actionOnMessage);
  }
}

exports.onMessage = function onMessage(command, args, message) {
  extensionActionsOnMessage.forEach(actionOnMessage => actionOnMessage(command, args, message));
}