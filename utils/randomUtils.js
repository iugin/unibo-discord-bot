var crypto = require('crypto');
var sjcl = require('sjcl');
var Base64 = require('js-base64').Base64;

var buf = crypto.randomBytes(1024 / 8) // 128 bytes
buf = new Uint32Array(new Uint8Array(buf).buffer)
sjcl.random.addEntropy(buf, 1024, "crypto.randomBytes")

function next() {
  var random = sjcl.random.randomWords(1, 6)[0];
  random = Base64.encodeURI(random);
  return random;
}

exports.next = next;